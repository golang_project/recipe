package middleware

import (
	"github.com/gin-gonic/gin"
	"github.com/rs/xid"
	"recipe/constant"
	logger2 "recipe/logger"
)

func LoggerMdw() gin.HandlerFunc {
	return func(c *gin.Context) {
		correlationId := xid.New().String()
		c.Set(constant.CORRELATION_ID_CTX_KEY, correlationId)
		logger := logger2.Get(c)

		logger.Info("Handle message start befor c.Next")
		c.Next()
		logger.Info("After message after c.Next")
	}
}
