package main

import (
	"context"
	"github.com/gin-gonic/gin"
	"net/http"
	"recipe/logger"
	"recipe/middleware"
)

func healthcheckHandler(c *gin.Context) {
	l := logger.Get(c)
	l.Info("Get healthcheck service")
	c.JSON(http.StatusOK, gin.H{
		"status": "200",
	})
}

func main() {
	r := gin.Default()
	r.Use(middleware.LoggerMdw())
	l := logger.FromCtx(context.TODO())
	l.Info("Start service on port 8002")
	r.GET("/healthcheck", healthcheckHandler)

	r.Run(":8002")
}
